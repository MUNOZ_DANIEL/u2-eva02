<%-- 
    Document   : lista_registros e implementa acceso modificar y eliminar
    Created on : Apr 24, 2021, 6:45:56 PM
    Author     : danielmunoz
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="root.entity.Proveedores"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Proveedores> pro_registros = (List<Proveedores>)request.getAttribute("pro_registros");
    Iterator<Proveedores> it_registros = pro_registros.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proveedores</title>
    </head>
    <body>
        <form  name="form" action="ControllerApp" method="POST">
            <table border = "1">
                <thead>
                    <tr>
                        <th>ID Proveedor</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Teléfono</th>
                        <th>Dirección</th>
                    </tr>
                </thead>
                <tbody>
                    <%while (it_registros.hasNext()) {
                        Proveedores displayLog = it_registros.next();%>
                    }
                    <tr>
                        <td><%= displayLog.getProIdProveedor()%></td>
                        <td><%= displayLog.getProNombre()%></td>
                        <td><%= displayLog.getProApellido()%></td>
                        <td><%= displayLog.getProTelefono()%></td>
                        <td><%= displayLog.getProDireccion()%></td>
                        <td><input type="radio" name="selection" value="<%= displayLog.getProIdProveedor()%>" /></td>
                    </tr>
                </tbody>
            </table>
                    <button type="submit" name="action" value="editar"> Editar Registro </button> 
                    <button type="submit" name="action" value="eliminar"> Eliminar Registro </button> 
        </form>

    </body>
</html>
