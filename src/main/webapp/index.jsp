<%-- 
    Document   : index
    Created on : Apr 24, 2021, 4:54:19 PM
    Author     : danielmunoz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            form {
                /* Centrar el formulario en la página */
                margin: 0 auto;
                width: 500px;
                /* Esquema del formulario */
                padding: 1em;
                border: 1px solid #CCC;
                border-radius: 1em;
            }
        </style>
        <title>Home Apps</title>
    </head>
    <h3 style="text-align: center">REGISTRO DE PROVEEDORES</h3>
    <form name="form" action="ControllerApp" method="POST">
        <div>
            ID Proveedor : <input type="text" name="pro_id_proveedor" id="pro_id_proveedor" placeholder="123456789" size=50 autofocus  /><br>
        </div>
            Nombre    : <input type="text" name="pro_nombre" id="pro_nombre" placeholder="Ethan" size="50"  /><br>
        <div>
        </div>
        Apellido  : <input type="text" name="pro_apellido" id="pro_apellido" placeholder="Krofforn" size="50"  /><br>
        Telefono  : <input type="text" name="pro_telefono" id="pro_telefono" placeholder="56991930411" size="50"  /><br>
        Dirección : <input type="text" name="pro_direccion" id="pro_direccion" placeholder="1740 NW River Will, OR, USA" size="50" /><br>
        <div style="text-align: center">
            <br>
            <button type="submit" name="action" value="guardar"> Guardar Registro </button>
            <button type="submit" name="action" value="listar"> Ver Registros </button>
        </div>
    </form>
    <footer>
        <div style="text-align: center">
                <br />
                <p>
                    Desarrollado por el alumno DANIEL MUÑOZ PASTENE<br>
                    Versión 1.0 con fecha April 2021<br>
                </p><br>
                <img src="LOGO_CIISA_apaisado_PNG.png" width="120" height="70" alt="LOGO_CIISA_apaisado_PNG"/><br>
                <p>Taller Aplicaciones Empresariales - IC201IECIREOL | Sección 50 | Evaluación #2 Unidad 2</p>                        
        </div>
    </footer>
</html>
